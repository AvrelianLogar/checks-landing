<?php
ini_set('display_errors', '1');
require $_SERVER['DOCUMENT_ROOT'] . '/business_ru_api.php';
$data = json_decode(file_get_contents('php://input'), true);
$data['phone'] = str_replace([' ', '(', ')', '-'], '',$data['phone']);
sendEmail($data);
sendDeal($data);

function sendEmail($data)
{
    $cutTimestamp = date('Y-m-d H:i:s');
    $mailTo = 'rozniccca@mail.business.ru';
    $subject = 'Заявка';
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "From: Онлайн-чеки\r\n";
    $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

    $styles = [
        'fs' => 'style="border: 1px solid black; font-size: 24px; font-family: Calibri, sans-serif; background-color: #d2e4ff; font-weight: bold; color: #34639c; width: 30%;"',
        'ss' => 'style="border: 1px solid black; font-size: 24px; font-family: Calibri, sans-serif; font-weight: 400; color: #373737; width: 70%;"'
    ];

    switch (intval($data['utm']['campaign'])) {
        case 32075466:
            $campagnText = 'РК_Поиск';
            break;
        case 32149207:
            $campagnText = 'РК_Сети';
            break;
        case 137732:
            $campagnText = 'Рассылка';
            break;
        default:
            $campagnText = 'Не указано';
            break;
    }

    $message = '<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Заявка с лендинга "Онлайн-Чеки"</title>
</head>
<body>
<h3>Заявка с лендинга "Онлайн-Чеки"</h3>
<table style="width: 100%">
    <tr>
        <td '.$styles['fs'].' class="fs">Лендинг</td>
        <td '.$styles['ss'].' class="ss">'.$data['landing'].'</td>
    </tr>
    <tr>
        <td '.$styles['fs'].' >Имя</td>
        <td '.$styles['ss'].'>' . $data['name'] . '</td>
    </tr>
    <tr>
        <td '.$styles['fs'].' >Телефон</td>
        <td '.$styles['ss'].'>' . $data['phone'] . '</td>
    </tr>
    <tr>
        <td '.$styles['fs'].' >Почтовый ящик</td>
        <td '.$styles['ss'].'>' . $data['email'] . '</td>
    </tr>
    <tr>
        <td '.$styles['fs'].' >Партнер</td>
        <td '.$styles['ss'].'>' . $data['partner'] . '</td>
    </tr>
    <tr>
        <td '.$styles['fs'].' >Источник перехода (utm_source)</td>
        <td '.$styles['ss'].'>' . $data['utm']['source'] . '</td>
    </tr>
    <tr>
        <td '.$styles['fs'].' >Тип трафика (utm_medium)</td>
        <td '.$styles['ss'].'>' . $data['utm']['medium'] . '</td>
    </tr>
    <tr>
        <td '.$styles['fs'].' >Название рекламной компании (utm_campaign)</td>
        <td '.$styles['ss'].'>' . $campagnText . '</td>
    </tr>
    <tr>
        <td '.$styles['fs'].' >Ключевая фраза (utm_term)</td>
        <td '.$styles['ss'].'>' . $data['utm']['term'] . '</td>
    </tr>
    <tr>
        <td '.$styles['fs'].' >Дополнительная информация (utm_content)</td>
        <td '.$styles['ss'].'>' . $data['utm']['content'] . '</td>
    </tr>
    <tr>
        <td '.$styles['fs'].' >utm_from</td>
        <td '.$styles['ss'].'>' . $data['utm']['from'] . '</td>
    </tr>
    <tr>
        <td '.$styles['fs'].' >Дата</td>
        <td '.$styles['ss'].'>' . $cutTimestamp . '</td>
    </tr>
</table>
</body>
</html>';
    mail('roznica@mail.business.ru', $subject, $message, $headers);
}

function sendDeal($data)
{
    $address = 'https://checki.business.ru/';
    $secretKey = 'jxT9KXrLwkXBpB3SZvkJt2tOsraBB1CK';
    $appID = '931180';

    $data['phone'] = str_replace([' ', '(', ')'], '',$data['phone']);
    $curDate = date('d.m.Y H:i:s');

    $dealName = $data['name'] . ' ' . $data['phone'] . ' ' . $data['email'] . ' ' . $data['partner'];
    $authorID = 1120445;
    $organizationID = 75519;
    $importance = 2;

    $api = new Business_ru_api_lib($appID, '', $address);

    $dealDescription = 'Имя: ' . $data['name'] . ' || Телефон: ' . $data['phone'] . ' || Email: ' . $data['email'] . ' || Партнер:' . $data['partner'] . ' || UTM ' . print_r($data['utm']);

    $api->setSecret($secretKey);

    $token = $api->repair();
    $token = $token['token'];
    $api->setToken($token);
    $request = $api->request(
        'post',
        'deals',
        [
            'name' => $dealName,
            'author_employee_id' => $authorID,
            'responsible_employee_id' => $authorID,
            'organization_id' => $organizationID,
            'description' => $dealDescription,
            'importance' => $importance,
            'time_control' => $curDate,
            '702868' => $data['phone'],
            '949888' => $data['email'],
        ]
    );
    var_dump($request);
}