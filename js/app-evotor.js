var urlLocation = new URL(window.location.href);

var state = true;

var urlData = {
    partner: urlLocation.searchParams.get("partner"),
    source: urlLocation.searchParams.get("utm_source"),
    medium: urlLocation.searchParams.get("utm_medium"),
    campaign: urlLocation.searchParams.get("utm_campaign"),
    term: urlLocation.searchParams.get("utm_term"),
    content: urlLocation.searchParams.get("utm_content"),
    from: urlLocation.searchParams.get("from"),
}

if (urlData.partner) {
    Cookies.set('q_partner', urlData.partner)
};
if (urlData.source) {
    Cookies.set('q_source', urlData.source)
};
if (urlData.medium) {
    Cookies.set('q_medium', urlData.medium)
};
if (urlData.campaign) {
    Cookies.set('q_campaign', urlData.campaign)
};
if (urlData.term) {
    Cookies.set('q_term', urlData.term)
};
if (urlData.content) {
    Cookies.set('q_content', urlData.content)
};
if (urlData.from) {
    Cookies.set('q_from', urlData.from)
};

console.log(document.cookie);

var scrollToVariants = false;

$(document).ready(function () {
    $.mask.definitions['~'] = '[+-]';
    $('#inputPhone').mask('+7(999) 999-99-99');

    setTimeout(checkModal, 40000);

    //проверяем открыто какое-то модальное окно
    function checkModal() {
        if (!$('#exampleModal').hasClass('show') && !$('#ModalBoxLast').hasClass('show')) {


            $('#exampleModalLabel').text('Оставьте заявку и мы проведем для Вас квалифицированную консультацию по 54-ФЗ');
            $('#exampleModal input[type=hidden]').val('Заявка на квалифицированную консультацию по 54-ФЗ');
            $('#exampleModal').modal('show');
        }
    }
});


$('.button-invoice, .button-callback').click(function () {
    var el = $(this).attr('data-namebutton');
    console.log(el);

    $('#form-modal input[type="hidden"]').val(el);
    $('#exampleModalLabel').text('Оставьте заявку');
});

$('#form-submit').click(function () {


    var pattern = /^( +)?((\+?7|8) ?)?((\(\d{3}\))|(\d{3}))?( )?(\d{3}[\- ]?\d{2}[\- ]?\d{2})( +)?$/;
    var patternemail = /^([a-zA-Z0-9_\.-]+)@([a-zA-Z0-9_\.-]+)\.([a-zA-Z\.]+)$/;

    var url = new URL(window.location.href);

    var data = {
        landing: $('#landing').attr('class'),
        name: $('#inputUsername').val(),
        phone: $('#inputPhone').val(),
        email: $('#inputEmail').val().toLowerCase(),
        partner: Cookies.get('q_partner'),
        utm: {
            source: Cookies.get('q_source'),
            medium: Cookies.get('q_medium'),
            campaign: Cookies.get('q_campaign'),
            term: Cookies.get('q_term'),
            content: Cookies.get('q_content'),
            from: Cookies.get('q_from'),
        }
    };

    if (data.name === "") {
        $('.btn-wrong').text('Напишите своё имя').show('100').delay('3000').fadeOut();
    }
    else if (data.phone === "") {
        $('.btn-wrong').text('Напишите свой телефон').show('100').delay('3000').fadeOut();
    }
    else if (data.phone.search(pattern) !== 0) {
        $('.btn-wrong').text('Телефон написан неверно').show('100').delay('3000').fadeOut();
    }
    else if (data.email === "") {
        $('.btn-wrong').text('Напишите свой email').show('100').delay('3000').fadeOut();
    }
    else if (data.email.search(patternemail) !== 0) {
        $('.btn-wrong').text('Email написан неверно').show('100').delay('3000').fadeOut();
    } else {
        if (state) {
            $.ajax({
                url: 'https://online-check.business.ru/send.php',
                type: "POST",
                data: JSON.stringify(data),
                success: function (result) {
                    state = false;
                    $('#success_send').show();
                    console.log(result);
                    yaCounter45435954.reachGoal('order');
                }
            });
        }
    }
});

//отправляем данные с баннера последней надежды
$('#form-submitlast').click(function () {

    var pattern = /^( +)?((\+?7|8) ?)?((\(\d{3}\))|(\d{3}))?( )?(\d{3}[\- ]?\d{2}[\- ]?\d{2})( +)?$/;

    var url = new URL(window.location.href);

    var data = {
        phone: $('#inputPhoneLast').val(),
        landing: $('#landing').attr('class'),
        partner: Cookies.get('q_partner'),
        utm: {
            source: url.searchParams.get("utm_source"),
            medium: url.searchParams.get("utm_medium"),
            campaign: url.searchParams.get("utm_campaign"),
            term: url.searchParams.get("utm_term"),
            content: url.searchParams.get("utm_content"),
            from: url.searchParams.get("from")
        }
    };

    if (data.phone === "") {
        $('.btn-wrong2').text('Напишите свой телефон').show('100').delay('3000').fadeOut();
    }
    else if (data.phone.search(pattern) !== 0) {
        $('.btn-wrong2').text('Телефон написан неверно').show('100').delay('3000').fadeOut();
    }

    else {
        if (state) {
            $.ajax({
                url: 'https://online-check.business.ru/send.php',
                type: "POST",
                data: JSON.stringify(data),
                success: function (result) {
                    state = false;
                    $('#success_send_last').show();
                    yaCounter45435954.reachGoal('lastChanseSend');
                }
            });
        }
    }
});