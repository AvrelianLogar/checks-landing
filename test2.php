<?php
$message = '<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Заявка с лендинга "Онлайн-Чеки"</title>
    <style>
        h1{
            font-family: Calibri, sans-serif;
            color: #004a79;
        }
        table {
            width: 100%;
        }

        td {
            border: 1px solid #004a79;
            font-size: 24px;
            font-family: Calibri, sans-serif;
        }

        .fs {
            background-color: #d2e4ff;
            font-weight: bold;
            color: #34639c;
            width: 30%;
        }

        .ss {
            width: 70%;
            font-weight: 400;
            color: #373737;
        }

    </style>
</head>
<body>
<h3>Заявка с лендинга "Онлайн-Чеки"</h3>
<table>
    <tr>
        <td class="fs">Имя</td>
        <td class="ss">' . $data['name'] . '</td>
    </tr>
    <tr>
        <td class="fs">Телефон</td>
        <td class="ss">' . $data['phone'] . '</td>
    </tr>
    <tr>
        <td class="fs">Почтовый ящик</td>
        <td class="ss">' . $data['email'] . '</td>
    </tr>
    <tr>
        <td class="fs">utm_source</td>
        <td class="ss">' . $data['utm']['source'] . '</td>
    </tr>
    <tr>
        <td class="fs">utm_medium</td>
        <td class="ss">' . $data['utm']['medium'] . '</td>
    </tr>
    <tr>
        <td class="fs">utm_campaign</td>
        <td class="ss">' . $data['utm']['campaign'] . '</td>
    </tr>
    <tr>
        <td class="fs">utm_term</td>
        <td class="ss">' . $data['utm']['term'] . '</td>
    </tr>
    <tr>
        <td class="fs">utm_content</td>
        <td class="ss">' . $data['utm']['content'] . '</td>
    </tr>
    <tr>
        <td class="fs">utm_from</td>
        <td class="ss">' . $data['utm']['from'] . '</td>
    </tr>
    <tr>
        <td class="fs">Дата</td>
        <td class="ss">' . $cutTimestamp . '</td>
    </tr>
</table>
</body>
</html>';